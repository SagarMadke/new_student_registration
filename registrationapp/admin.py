from django.contrib import admin
from .models import Registration, ImageGallary, Pic_Attachment
from .forms import RegistrationForm, ImageGallaryForm
#from django.contrib.admin.widgets import AdminFileWidget
#from django.utils.safestring import mark_safe
#from sorl.thumbnail import get_thumbnail


"""class AdminImageWidget(AdminFileWidget):
  def render(self, name, value, attrs=None):
    output = []
    if value and getattr(value, "url", None):
      t = get_thumbnail(value,'80x80')
      output.append('<img src="{}">'.format(t.url))
    output.append(super(AdminFileWidget, self).render(name, value, attrs))
    return mark_safe(u''.join(output))"""



# Register your models here.
class RegistrationModelAdmin(admin.ModelAdmin):
	list_display = ["id", "profile_image", "first_name", "last_name", "resume"]
	search_fields = ["first_name", "last_name"]
	class Meta:
		model = Registration

admin.site.register(Registration, RegistrationModelAdmin)
admin.site.register(ImageGallary)
admin.site.register(Pic_Attachment)