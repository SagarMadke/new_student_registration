from django.db import models
from django.utils import timezone
from django import forms

CHOICES=[
	(True,'Male'),
	(False,'Female')
	]
city=[
	('Pune','Pune'),
	('Latur','Latur'),
	('Osmanabad','Osmanabad'),
	('Solapur','Solapur')
	]
state=[
	('MH','Maharashtra'),
	('GJ','Gujarat'),
	('PN','Punjab')
	]
country=[
	('IN','India'),
	('SA','South Africa'),
	('UK','United Kingdom')
	]
course=[
	('php','Php'),
	('python','Python'),
	('html','Html'),
	('css','css'),
	('angular','Angular')
	]
class Registration(models.Model):
	first_name = models.CharField(max_length=100)	
	last_name = models.CharField(max_length=100)
	email = models.EmailField(max_length=50)		
	profile_image= models.ImageField(upload_to="images/")	
	resume= models.FileField(upload_to="Uploads/", max_length=100)	
	birth_date = models.DateField(default=timezone.now)	
	bio=models.TextField()	
	gender= models.BooleanField(choices=CHOICES)	
	state= models.CharField(max_length=2, choices=state)	
	country= models.CharField(max_length=2, choices=country)	
	technology = models.CharField(max_length=10, choices=course)
	#widget=models.TextInput(attrs={'class':'datepicker'})
	def __str__(self):
		return self.first_name

	def get_absolute_url(self):
		return "%s/" %(self.id)

class ImageGallary(models.Model):
	title = models.CharField(max_length=100)
	upload_images = models.FileField(upload_to="images/")

	def __str__(self):
		return self.title

class RegistrationResidency(models.Model):
	first_name = models.CharField(max_length=100)	
	last_name = models.CharField(max_length=100)		
	birth_date = models.DateField(default=timezone.now)		
	gender= models.BooleanField(choices=CHOICES)	
	state= models.CharField(max_length=2, choices=state)	
	country= models.CharField(max_length=2, choices=country)
	city=models.CharField(max_length=10, choices=city)
	technology = models.CharField(max_length=10, choices=course)
	#widget=models.TextInput(attrs={'class':'datepicker'})
	
	def __str__(self):
		return self.first_name 


class Pic_Attachment(models.Model):
    image_id = models.CharField(max_length=100)
    image_name = models.CharField(max_length=100)
    attachment = models.FileField(upload_to="images/")

    def __str__(self):
    	return self.image_name

