from django import forms
from .models import Registration, ImageGallary

class RegistrationForm(forms.ModelForm):
	class Meta:
		model = Registration
		fields = [
			"first_name",
			"last_name",
			"email",
			"birth_date",
			"profile_image",
			"resume",
			"bio",
			"gender",
			"state",
			"country",
			"technology",
		] 

class UpdateForm(forms.ModelForm):
	class Meta:
		model = Registration
		fields = [
			"email",
			"profile_image",
			"resume",
			"bio",
			"state",
			"country",
			"technology",
		]

"""class RegistrationResidencyForm(forms.ModelForm):
	class Meta:
		model = RegistrationResidency
		fields = [
			"first_name",
			"last_name",
			"email",
			"birth_date",
			"profile_image",
			"resume",
			"bio",
			"gender",
			"country",
			"state",
			"city",
		]"""

class ImageGallaryForm(forms.ModelForm):
	class Meta:
		model = ImageGallary
		fields = ["title", "upload_images", ]