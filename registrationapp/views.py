from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from .forms import RegistrationForm, UpdateForm, ImageGallaryForm
from .models import Registration, ImageGallary, Pic_Attachment
# Create your views here.
def home(request):
	return HttpResponse("<h1>Wel-come to Digital Prizm</h1>")

#@login_required
def register_view(request):
	form = RegistrationForm(request.POST or None, request.FILES or None)
	if form.is_valid(): 
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Registered successfully...")
		#return HttpResponseRedirect("view_details.html")
	else:
		messages.error(request, "Registration failed...")
	#if request.method == "POST":
		#print (request.POST.get("first_name"))
	#context = {
		#"form" : form,
	#}

	return render(request, "registration_form.html", {'form' : form})

def register_update(request, id=None):
	instance = get_object_or_404(Registration, id=id)
	form = RegistrationForm(request.POST or None, request.FILES or None, instance=instance)
	print (instance.id)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Updated successfully...")
		return HttpResponseRedirect(instance.get_absolute_url())
	else:
		messages.error(request, "Record updation failed...")
	return render(request, "update_form.html", {'form' : form})

def register_delete(request, id=None):
	instance = get_object_or_404(Registration, id=id)
	instance.delete()
	messages.success(request, "Record deleted successfully...")
	return redirect('view_details')
	#return HttpResponseRedirect(instance.get_absolute_url())
	return render(request, "view_details.html", {})


def view_details(request):
	context = Registration.objects.all()
	#queryset_list = Registration.objects.all()
	query = request.GET.get("q")
	print (query)
	if query :
		context = context.filter(first_name__icontains=query)
	#paginator = paginator(context, 5)
	
	return render(request, 'view_details.html', {'context':context })

def multiple_image_upload(request):
	if request.method == "POST" :
		files = request.FILES.getlist('files')
		image_name = request.POST['image_name']
		print (files)
		print (image_name)

		for f in files:
			instance = Pic_Attachment(image_name = image_name , attachment = f)
			instance.save()
			messages.success(request, "Images uploaded successfully....")
	return render(request, 'multiple_images.html', {})
	#return HttpResponse("Files uploaded...") + str(count),


def list_demo(request):	
	#if request.method == "POST":		
	Fname= request.POST['fname']		
	"""Lname=request.POST['last_name']				
	dob=request.POST['birth_date']				
	state=request.POST['state']		
	city=request.POST['city']		
	gender=request.POST['gender']		
	tech=request.POST['technology']	"""			

	print (Fname)		
	"""print (state)		
	print (city)		
	print (gender)		
	print (tech)		

	for s in resume:			
		se=s		
	for p in pic:			
		pi=p			
	instance=RegistrationResidency(FirstName=Fname,				
		LastName=Lname,Myprofile=pi,Myresume=se,				
		dob=dob,Bio=bio,Country=country,State=state,				
		city=city,Gender=gender,Tech=tech)			
	instance.save()"""
	return render(request, 'residency_region.html', {})


	"""form = ImageGallaryForm(request.POST or None, request.FILES or None)
	#context = ImageGallary.objects.all()
	instance = form.save(commit=False)
	instance.save()
	messages.success(request, "Uploaded successfully...")
	return render(request, 'multuple_images.html', {'form' : form})"""