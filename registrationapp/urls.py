from django.conf.urls import include, url
from django.contrib import admin
from .forms import RegistrationForm, ImageGallaryForm
from .models import Registration, ImageGallary
from registrationapp import views

urlpatterns = [
	url(r'^$', views.register_view, name="register_view" ),
    url(r'^register/', views.register_view, name="register_view" ),
    url(r'^(?P<id>\d+)/update/', views.register_update, name="register_update"),
    url(r'^(?P<id>\d+)/delete/', views.register_delete, name="register_delete"),
    url(r'^details$', views.view_details, name="view_details"),
    url(r'^images/', views.multiple_image_upload, name="multiple_image_upload"),
    url(r'^list/', views.list_demo, name="list_demo"),
    url(r'^admin/', admin.site.urls),
]
